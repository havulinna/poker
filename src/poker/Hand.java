package poker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Hand {

    private List<Card> cards = new ArrayList<>();

    public Hand(Card... cards) {
        for (Card c : cards) {
            this.cards.add(c);
        }
    }

    public boolean isRoyalFlush() {
        return containsAce() && containsKing() && isStraightFlush();
    }

    public boolean isStraightFlush() {
        return isStraight() && isFlush();
    }

    public boolean isFourOfAKind() {
        return rankFrequencies().containsValue(4);
    }

    public boolean isFullHouse() {
        return !rankFrequencies().containsValue(1);
    }

    public boolean isFlush() {
        Suit suit = cards.get(0).getSuit();
        for (Card c : cards) {
            if (c.getSuit() != suit) {
                return false;
            }
        }
        return true;
    }

    public boolean isStraight() {
        List<Card> copy = new ArrayList<>(cards);
        Collections.sort(copy);

        if (copy.get(0).isAce() && copy.get(copy.size() - 1).isKing()) {
            // Potential royal straight, we can omit the ace
            copy.remove(0);
        }

        for (int i = 0; i < copy.size(); i++) {
            if (copy.get(i).getRank() != copy.get(0).getRank() + i) {
                return false;
            }
        }
        return true;
    }

    public boolean isThreeOfAKind() {
        return rankFrequencies().containsValue(3);
    }

    public boolean isTwoPairs() {
        return Collections.frequency(rankFrequencies().values(), 2) == 2;
    }

    public boolean isPair() {
        return rankFrequencies().containsValue(2);
    }

    /**
     * Constructs a map which stores the information about how many cards of each
     * rank exist in the hand.
     * 
     * For example, for a hand that has two aces and three kings, there will be two
     * entries: key '1' with value '2' and key '13' with value '3'.
     * 
     * @return
     */
    private Map<Integer, Integer> rankFrequencies() {
        Map<Integer, Integer> ranks = new TreeMap<>();
        for (Card c : cards) {
            if (ranks.containsKey(c.getRank())) {
                ranks.put(c.getRank(), ranks.get(c.getRank()) + 1);
            } else {
                ranks.put(c.getRank(), 1);
            }

        }
        return ranks;
    }

    private boolean containsAce() {
        Map<Integer, Integer> ranks = rankFrequencies();
        return ranks.containsKey(1) && ranks.get(1) > 0;
    }

    private boolean containsKing() {
        Map<Integer, Integer> ranks = rankFrequencies();
        return ranks.containsKey(13) && ranks.get(13) > 0;
    }
}
